import os
import numpy as np
import numpy.matlib

import pandas as pd

fp_to_dataset = '../dataset/PlantVillage/RGB'
fp_to_labels = '../dataset/plantvillage/dataset_labels2.csv'
list_of_plants = ['pepper', 'potato', 'tomato']
list_of_diseases = ['healthy', 'bacterial_spot', 'early_blight', 'late_blight', 'target_spot', 'mosaic_virus', 'yellowleaf', 'leaf_mold', 'septoria_leaf_spot', 'spider_mites']

list_of_directories = os.listdir(fp_to_dataset)

print("Found {} subdirectories.\n".format(len(list_of_directories)))

data = []

# iterate through directories
for directory in list_of_directories:
	
	## Extract plant and disease type from directory name
	plantType = ''
	diseaseType = ''
	
	for plant in list_of_plants:
		if directory.lower().find(plant) != -1:
			plantType = plant
	
	for disease in list_of_diseases:
		if directory.lower().find(disease) != -1:
			diseaseType = disease
	
	# get list of filenames from directory
	image_files = os.listdir(fp_to_dataset + '/' + directory)
	temp = []
	for file in image_files:
		if ".jpg" in file.lower():
			temp.append(file)
	image_files = temp
	print("Found {} images in {}.\n".format(len(list_of_directories), len(image_files)))
	
	# append filenames to plant types and disease status/type
	this_data_chunk = np.concatenate((np.array(image_files)[:,np.newaxis],
		np.matlib.repmat(plantType,len(image_files),1),
		np.matlib.repmat(diseaseType,len(image_files),1)
		),axis=1)
	data.append(this_data_chunk)

# concatenate numpy arrays
data = np.concatenate(data,axis=0)
# convert to pandas df and save as .csv
df = pd.DataFrame(data,columns=['filename','plant','disease'])

print("Found a total of{} samples.".format(len(df)))
df.to_csv(fp_to_labels)