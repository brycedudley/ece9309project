# Do your includes 
# Include libraries that will be needed throughout the project for image processing

# Simple Insight Segmentation and Registration Toolkit
import SimpleITK as sitk

# Maltab plotting 
import matplotlib.pyplot as plt

# array manipulation 
import numpy as np

#Calculating block times 
import timeit

import os

# good for calucating region properties for images (maybe needed later so I haven't deleted it) 
from skimage.measure import label , regionprops
from skimage import data
from skimage.color import rgb2hsv
from skimage import filters
from skimage import feature 

import pandas as pd
import imageio

from scipy import ndimage as ndi
from scipy.cluster import vq

class RemoveBackgroundController: 
    # define initation 
    def __init__(self,filename,name,path_to_dataset):
        
        # get directories to parse through 
        list_of_directories = os.listdir(path_to_dataset)
        
        #initiate a dynamic array for images 
        self.array_rgb = {}
        self.hsv = {}
        self.name = {}
        self.filename = {}
        self.channel = {}
        self.working = {}
        
        self.working = 0
        
        # read the rgb images 
        for directory in list_of_directories:
            try: 
                self.array_rgb = sitk.GetArrayFromImage(sitk.ReadImage(path_to_dataset+ '/' + directory + '/' + filename))
                self.name = name
                self.filename = filename
                
                # split the color channels 
                # channel[0] = red, channel[1] = green, channel[2]= blue 
                for i in range(0,3):
                    self.channel[i] = self.array_rgb[:,:,i]
                    
                # weighted average of rgb as grayscale --> 0.299 red + 0.587 green + 0.144 blue 
                self.channel[3] =  0.299*self.channel[0] + 0.587*self.channel[1] + 0.144*self.channel[2]
                
                # hsv transform 
                self.hsv = rgb2hsv(self.array_rgb)
                
                # channel[4] = Hue, channel[5] = saturation, channel[6] = value 
                self.channel[4] = self.hsv[:,:,0]
                self.channel[5] = self.hsv[:,:,1]
                self.channel[6] = self.hsv[:,:,2]
                
                # if all was done properly label it as working
                self.working =1
                
            except:
                #print("Error in File:"+self.name+"/"+self.filename)
                pass

    def RemoveBackgroundNick(self):
        
        # Using Nick method
        
        # define otsu filter
        otsu_filter = sitk.OtsuThresholdImageFilter() 
        entropy_filter = sitk.MaximumEntropyThresholdImageFilter()
        
        inside_value = 255 #set inside values to 1 
        outside_value = 0 #set outside values to 0 

        # Pass the values to the otsu filter
        otsu_filter.SetInsideValue(inside_value) 
        otsu_filter.SetOutsideValue(outside_value)
       
         # Pass the values to the otsu filter
        entropy_filter.SetInsideValue(inside_value) 
        entropy_filter.SetOutsideValue(outside_value)
        
        # define holefillfilter
        holeFillFilter = sitk.GrayscaleFillholeImageFilter()
        holeFillFilter.FullyConnectedOff()

        self.nick_otsu_image1 = {}
        self.nick_hole_image1 = {}
        self.nick_overlay_image1 = {}
        
        # Use otsu on blue band 
        self.nick_otsu_image1 = otsu_filter.Execute(sitk.GetImageFromArray(self.channel[2]))
        self.nick_hole_otsu_image1 = sitk.Cast(sitk.RescaleIntensity(holeFillFilter.Execute(self.nick_otsu_image1)),sitk.sitkUInt8)
        
        self.nick_overlay_image1 = sitk.LabelOverlay(sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(self.channel[3])),sitk.sitkUInt8),self.nick_hole_otsu_image1,opacity = 0.35)
        
    def RemoveBackgroundBo(self):
        
        # Using Bo
        
        # define otsu filter
        otsu_filter = sitk.OtsuThresholdImageFilter() 
        entropy_filter = sitk.MaximumEntropyThresholdImageFilter()
        
        inside_value = 255 #set inside values to 1 
        outside_value = 0 #set outside values to 0 
        
        # Pass the values to the otsu filter
        otsu_filter.SetInsideValue(inside_value) 
        otsu_filter.SetOutsideValue(outside_value)
        
        # Pass the values to the entropy filter
        entropy_filter.SetInsideValue(inside_value) 
        entropy_filter.SetOutsideValue(outside_value)
        
        self.bo_otsu_image1 = {}
        self.bo_otsu_image2 = {}
        self.bo_overlay_image1 = {}
        self.bo_overlay_image2 = {}
        
        # Use otsu on blue band 
        self.bo_otsu_image1 = otsu_filter.Execute(sitk.GetImageFromArray(self.channel[4]))
        self.bo_otsu_image2 = ~otsu_filter.Execute(sitk.GetImageFromArray(self.channel[5]))
        
        self.bo_overlay_image1 = sitk.LabelOverlay(sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(self.channel[3])),sitk.sitkUInt8),self.bo_otsu_image1,opacity = 0.35)
        self.bo_overlay_image2 = sitk.LabelOverlay(sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(self.channel[3])),sitk.sitkUInt8),self.bo_otsu_image2,opacity = 0.35)
       
    def RemoveBackgroundBo2(self):
        # define all the variables Bo is using ! 
        self.n = {}
        self.bins = {}
        self.grad = {}
        self.lowerThresh = {}
        self.highTresh = {}
        self.medFilterThre2C = {}
        self.binary2 = {}
        self.binary3 = {}
        self.binary4 = {}
        self.medFilterThre2 = {}
        self.label_objects = {}
        self.nb_labels = {}
        self.size ={}
        self.mask_sizes = {}
        self.medFilterThre2C = {}
        self.morpho = {}
        self.final = {}
        self.final_image = {} 
        
        if(self.working ==1):
            # get the histogram in terms of 5 bins 
            self.n, self.bins = np.histogram(self.channel[4].ravel(), bins=5)

            # find the second derivative of bins (or where there is large changes in bins) 
            self.grad = np.gradient(self.n)

            # set the lower threshold and upper threshold as none to start
            self.lowerThresh = None
            self.highThresh = None

            # i should be in range of 0-5 really cuz we have 5 bins 
            for i in range(len(self.grad)):

                if i+1<len(self.n) and self.lowerThresh == None:
                    # define the first rise (aka the background) 
                    if self.grad[i] >10000 and self.n[i+1]>10000: # first rise
                        #print(bins[i+1], n[i+1])
                        # the lower threshold is now the background
                        self.lowerThresh = self.bins[i+1]
                        # the upper threshold is now the otsu value on hue channel 
                        self.highThresh = filters.threshold_otsu(self.channel[4]) 

                    if self.grad[i] < -5000 and self.n[i+1]>10000: # first fall (hue issue?)
                        self.lowerThresh = self.bins[i+1]
                        self.highThresh = filters.threshold_otsu(self.channel[4])
                        if self.lowerThresh > self.highThresh:
                            self.highThresh = self.bins[i+2]
                    self.medFilterThre2C = None

            # check to see if we have an answer        
            if self.lowerThresh is not None and self.highThresh is not None:
                #print(lowerThresh, highThresh)

                # do your upper and lower tresholding! 
                self.binary2 = np.where(self.channel[4] > self.lowerThresh, 255, 0)
                self.binary3 = np.where(self.channel[4] < self.highThresh, 255, 0)
                self.binary4 = self.binary2&self.binary3

                #median filter with kernel size of 3 (i believe it is square filter? (also used to reduce noise) 
                self.medFilterThre2= ndi.median_filter(self.binary4, size = 3)

                #reduce small pixel blocks (aka iterate through arrays to find objects larger than 20 pixels.) 
                self.label_objects, self.nb_labels = ndi.label(self.medFilterThre2)
                self.sizes = np.bincount(self.label_objects.ravel())
                self.mask_sizes = self.sizes > 200
                self.mask_sizes[0] = 0

                # get the final binary image
                self.medFilterThre2C = self.mask_sizes[self.label_objects]

                # close holes, this is only valid for healthy plants :) 
                self.morpho=ndi.morphology.binary_fill_holes(self.medFilterThre2C)

                # do the image + rgb multiplication 
                self.final = np.zeros((self.array_rgb.shape))
                for j in range(0,3):
                    self.final[:,:,j] = np.multiply(self.array_rgb[:,:,j],self.morpho)
                    self.final_image = sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(self.final)),sitk.sitkVectorUInt8)
                    self.final = sitk.GetArrayFromImage(self.final_image)

                # write the images to the correct file location! 
                imageio.imwrite("dataset/PlantVillage/LeafSegment/" + self.name + "/" +self.filename,self.final)
                
                
    def RemoveBackgroundEdgeNick(self,size,erode,channel_choice):
        
        closing_filter = sitk.BinaryMorphologicalClosingImageFilter()
        closing_filter.SetKernelType ( sitk.sitkBall )
        closing_filter.SetKernelRadius( [size,size])
        
        erode_filter = sitk.BinaryErodeImageFilter()
        
        # set the kerel type and how big the kernel is 
        erode_filter.SetKernelType ( sitk.sitkBall )
        erode_filter.SetKernelRadius( [erode,erode])
        
        
        # Edge Detection Algorithm -- my preferred method is canny currently 
        self.edge = {} 
        self.closed = {} 
        self.closed2 = {}
        self.erode = {}
        self.edge_n = {}
        self.edge_bin = {}
        self.final4 = {}
        
        # make object
        # canny_filter = sitk.CannyEdgeDetectionImageFilter()  

        # make matlab plots
        #plt.subplots(4,5,figsize=(15,15))

        self.edge = feature.canny(self.channel[channel_choice],5)
        # self.edge_n , self.edge_bin = np.histogram(self.channel[5])
        self.closed = closing_filter.Execute(sitk.GetImageFromArray(self.edge.astype(np.uint8)))
        self.erode = erode_filter.Execute(self.closed)
        self.erode = sitk.GetArrayViewFromImage(self.erode)
        
        self.final4 = np.zeros((self.array_rgb.shape))
        for j in range(0,3):
            self.final4[:,:,j] = np.multiply(self.array_rgb[:,:,j],self.erode>0)
            self.final4_image = sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(self.final4)),sitk.sitkVectorUInt8)
            self.final4 = sitk.GetArrayFromImage(self.final4_image)
            
    
    def RemoveBackgroundRegionGrowingNick(self):
        self.region1 = {}
        self.region2 = {}
        self.final2 = {}
        self.final3 = {}
        
        self.region1 = sitk.ConfidenceConnected(sitk.GetImageFromArray(self.channel[4]), seedList=[(125,125)],
                                numberOfIterations=10,
                                multiplier=2.5,
                                initialNeighborhoodRadius=5,
                                replaceValue=255)
        self.region2 = sitk.ConfidenceConnected(sitk.GetImageFromArray(self.channel[5]), seedList=[(125,125)],
                                numberOfIterations=10,
                                multiplier=2,
                                initialNeighborhoodRadius=5,
                                replaceValue=255)
        
        self.final2 = np.zeros((self.array_rgb.shape))
        for j in range(0,3):
            self.final2[:,:,j] = np.multiply(self.array_rgb[:,:,j],sitk.GetArrayViewFromImage(self.region1)>0)
            self.final2_image = sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(self.final2)),sitk.sitkVectorUInt8)
            self.final2 = sitk.GetArrayFromImage(self.final2_image)
                    
        self.final3 = np.zeros((self.array_rgb.shape))
        for j in range(0,3):
            self.final3[:,:,j] = np.multiply(self.array_rgb[:,:,j],sitk.GetArrayViewFromImage(self.region2)>0)
            self.final3_image = sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(self.final3)),sitk.sitkVectorUInt8)
            self.final3 = sitk.GetArrayFromImage(self.final3_image)
            
    def WatershedRemoveBackgroundNick(self, channel_choice):
        self.watershed_image = {} 
        
        watershed = sitk.MorphologicalWatershedImageFilter()
        self.watershed_image = watershed.Execute(sitk.GetImageFromArray(self.channel[channel_choice]))
        
<<<<<<< Updated upstream
    def RemoveBackgroundUnhealthyBo3(self):
        self.flattened = {}
        self.k_mean_out = {}
        self.centroid = {}
        self.label = {}
        self.medFilterThre2 = {}
        self.final_c = {}
        self.label_objects = {}
        self.nb_labels = {}
        self.sizes = {}
        self.mask_sizes = {}
        self.medFilterThre2C = {}
        self.forFinal = {}
        self.final5 = {}
        
        self.flattened = vq.whiten(self.channel[4].flatten())

        self.centroid, self.label = vq.kmeans2(self.flattened,2)
        self.k_mean_out = self.label.reshape(256,256)

        self.medFilterThre2= ndi.median_filter(self.k_mean_out, size = 3)

        #check if leaf is true or false, swtich to to T if F
        if self.medFilterThre2[128,128] ==0:
            #print("leaf is false")
            self.medFilterThre2[self.medFilterThre2 == 0] = 2
            self.final_c= self.medFilterThre2 -1
            #print(self.final_c.shape, self.final_c[128,128])
        else:
            self.final_c = self.medFilterThre2

        #reduce small pixel blocks

        self.label_objects, self.nb_labels = ndi.label(self.final_c)
        self.sizes = np.bincount(self.label_objects.ravel())
        self.mask_sizes = self.sizes > 200
        self.mask_sizes[0] = 0
        self.medFilterThre2C = self.mask_sizes[self.label_objects]

        self.forFinal = ndi.morphology.binary_fill_holes(self.medFilterThre2C)
        self.forFinal = ndi.morphology.binary_erosion(self.forFinal, iterations =2)
        
        self.final5 = np.zeros((self.array_rgb.shape))
        for j in range(0,3):
            self.final5[:,:,j] = np.multiply(self.array_rgb[:,:,j],self.forFinal>0)
            self.final5_image = sitk.Cast(sitk.RescaleIntensity(sitk.GetImageFromArray(self.final5)),sitk.sitkVectorUInt8)
            self.final5 = sitk.GetArrayFromImage(self.final5_image)
        
        # write the images to the correct file location! 
        imageio.imwrite("dataset/PlantVillage/LeafSegment/" + self.name + "/" +self.filename,self.final5)
        
        
=======
>>>>>>> Stashed changes
        