\documentclass[journal,twoside,web]{ieeecolor}
\usepackage{generic}
\usepackage{cite}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{algorithmic}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{float}

 
% FORMATTING
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

% mark header
\markboth{ECE9309 Progress Report}
{B. Dudley, N. Mitchell, B. Shan, and E. Simpson: Classification of Plant Disease Using Convolutional Neural Networks}

% document start
\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                 TITLE
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Classification of Plant Disease Using Convolutional Neural Networks \\
  \large ECE9309  Progress Report }
\author{B. Dudley, N. Mitchell, B. Shan, and E. Simpson }
\maketitle


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           ABSTRACT/META
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{IEEEkeywords}
Image Processing, Disease Detection, CNN
\end{IEEEkeywords}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              INTRODUCTION
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}
\label{sec:introduction}

% BACKGROUND
\subsection{Background}
Agricultural productivity plays an important role in society and the economy. Modern agricultural facilities have turned to optimization to compete with the ongoing need to increase the world food supply. Many of which have implemented large-scale growing operations equipped with the ability to monitor crops via a live images. These systems can be used to track the growth and health of produce. As crops may be close in proximity, infectious disease can become a large issue. Machine learning (ML) and computer vision can be applied to help solve this problem. ML models such as a convolutional neural network (CNN) can be trained to infer high level knowledge such as plant health using images of leaves.

% MOTIVATION
\subsection{Motivation}
 Preventing the spread of contagious disease is crucial amongst large-scale agricultural operations. The faster disease is recognized, the earlier it can be mitigated. Thus, increasing both the quantity and quality of produce output.

% PROBLEM
\subsection{Problem Definition}
Increase detection of plant-disease within agricultural production facilities that grow pepper, tomato, or potato.

% DELIVERABLES
\subsection{Expected Deliverables}
The following deliverables define the steps of implementation milestones and are listed in order as they appear within the project's development cycle. Each machine learning model described below will use a singular image of a leaf as their input parameter. For the purpose of the project, the model is expected to determine the following for the aforementioned fruits and vegetable.
\begin{enumerate}
	\item Batch image pre-processing to remove background noise
	\item CNN able to determine plant type
	\item CNN able to determine if a leaf is diseased or healthy
	\item CNN able to determine plant disease type
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              RELATED WORK
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Related Work}
\begin{enumerate}
% WORK 1
	\item In \cite{appleCNN}, Jiang \emph{et al.} developed a deep-CNN based approach to classify plant disease in apple leaves. The report emphasized the need to be able to handle images from ``field conditions''. Thus, images required pre-processing before being used with a CNN. An annotation-based method is first applied to the images that algorithmically specifies regions-of-interest (ROIs). These ROIs represent the coordinates of possible disease affected areas on the leaf. Images are then processed with various manipulations and fed into a 2-stage detection model. This consists of a prefix CNN and a multi-layered CNN that applies a complex technique to create feature maps. Experimental results displayed a high-degree of performance at a mean average precision rate of 78.80\% with a large dataset containing 26,377 images.
% WORK 2
	\item In \cite{mangoCNN}, Singh \emph{et al} attempt to identify anthracnose disease in the images of mango leaves. A straight-forward method featuring a single CNN combined with a pre-processing enhancement stage is described. Images are directly manipulated during pre-processing to increase contrast and reduce image size. All images are cropped and resized to a square 128x128 pixels. The CNN used contains six convolutional layers, a Rectified Linear Unit, three max-pooling layers, and two dense layers. This architecture is almost identical to the popular AlexNet \cite{alexNet} image classification model. Although the implementation described in the paper is relatively simple, testing results faired well with 97.13\%. However, this may be skewed due to over-fitting as only a small dataset of 2,200 images was used.
% WORK 3
  \item In \cite{maizeCNN}, Zhang \emph{et al} use a similar approach process to the aforementioned works; pre-processing, augmentation, ROI selection, and finally CNNs. Two popular image CNN architectures are used, Cifar10 and GoogLeNet \cite{GoogLeNet}. The paper describes optimization of these architectures by the use of a stochastic gradient descent algorithm. Ultimately this resulted in a slight adjustment to the network's base learning rate and batch size hyper parameters. Testing was conducted with a very small dataset of only 500 images. In conclusion, detection of 8 types of maize leaf diseases was achieved with an average accuracy of 98.8\%. This was attributed to increased pooling layers and hyper-parameter optimization.
\end{enumerate}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%           EXPLORATORY ANALYSIS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Exploratory Dataset Analysis}
\label{sec:explore}


%%%%%%%%%%%%%%%%        DATASET CLASSES       %%%%%%%%%%%%%%%%%%%%
\subsection{Dataset}
\label{sec:dataset}

The dataset consists of 20,639 samples where each sample consists of a single .jpg image. Samples are organized into 15 directories.  Directory names denote the sample class membership.  For example, there are 997 samples in the directory \textit{Pepper\_\_bell\_\_\_Bacterial\_spot}.

A script, \textit{generate\_labels.py} was written to traverse the directory structure and:
\begin{enumerate}
\item generate \textit{dataset\_labels.csv}, a file containing image sample file names and sample labels (based on directory names); and
\item copy all jpg files to a single, aggregated image directory.
\end{enumerate}
\textit{dataset\_labels.csv} consists of 3 columns: a pandas data-frame index; `filename', a string referencing the image file containing the sample data; `plant', a string describing sample's plant class membership; and `disease', a string describing the sample's disease/health class membership.  These columns can be converted to a one hot encoding at training/test time.

%%%%%%%%%%%%%%%%        PLANT CLASSES       %%%%%%%%%%%%%%%%%%%%
\subsection{Plant Classes}

\begin{figure}[!t]
\centerline{\includegraphics[width=\columnwidth]{figs/fig_plantID_dist.png}}
\caption{Distribution of plant classes in the dataset.  Note that there are many more tomatoes than potatoes or peppers.}
\label{fig:plantID}
\end{figure}

Figure \ref{fig:plantID} presents the distribution of plantID class membership across the dataset.

Of the 20,639 samples, 77.6\% are tomato leaves, 12.0\% are pepper leaves, and 10.4\% are potato leaves.  The distribution of plant samples is extremely uneven and heavily biased towards tomatoes.  This suggests that algorithms may have problems learning to differentiate between plant type classes, particularly between pepper and potato leaves.  It may be worth exploring artificially augmenting the pepper and potato leaf classes to create a more even distribution of classes.

%%%%%%%%%%%%%%%%        TOMATO LEAF DISEASE CLASSES       %%%%%%%%%%%%%%%%%%%%
\subsection{Tomato Leaf Disease Classes}

\begin{figure}[!t]
\centerline{\includegraphics[width=\columnwidth]{figs/fig_tomatoDiseaseID_dist.png}}
\caption{Distribution of tomato disease classes in the dataset.}
\label{fig:tomatoDiseaseID}
\end{figure}

The distribution of plant disease classes in the tomato samples is presented in Figure \ref{fig:tomatoDiseaseID}.

There are a total of 10 different classes in the set of tomato images.  Approximately 10\% of the tomato images are of healthy leaves, while the remaining 90\% are distributed among the nine disease classes.

In general, there is a broadly even distribution of class types, although there is an excess of yellow leaf samples and a deficiency of mosaic virus samples.

%%%%%%%%%%%%%%%%        PEPPER LEAF DISEASE CLASSES       %%%%%%%%%%%%%%%%%%%%
\subsection{Pepper Leaf Disease Classes}

\begin{figure}[!t]
\centerline{\includegraphics[width=\columnwidth]{figs/fig_pepperDiseaseID_dist.png}}
\caption{Distribution of pepper disease classes in the dataset.}
\label{fig:pepDiseaseID}
\end{figure}

Figure \ref{fig:pepDiseaseID} presents the distribution of diseaseID class membership across the pepper plantID class.

There are two classes in the set of pepper images.  Approximately 60\% of the pepper images represent a healthy leaf, and the remaining 40\% represent the bacterial spot class.

%%%%%%%%%%%%%%%%        POTATO  LEAF DISEASE CLASSES       %%%%%%%%%%%%%%%%%%%%
\subsection{Potato Leaf Disease Classes}

\begin{figure}[!t]
\centerline{\includegraphics[width=\columnwidth]{figs/fig_potatoDiseaseID_dist.png}}
\caption{Distribution of potato disease classes in the dataset.}
\label{fig:potDiseaseID}
\end{figure}

Figure \ref{fig:potDiseaseID} presents the distribution of diseaseID class membership across the potato plantID class.

There are three classes in the set of potato images.  Only 7\% of the pepper images represent a healthy leaf.  The remaining 93\% are approximately evenly split between the early and late blight classes.

%%%%%%%%%%%%%%%%%%%%%        IMPLICATIONS       %%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Implications}

The distribution of sample classes in the dataset is extremely uneven.  This may make it more challenging to learn to positively identify particular classes.

In particular, distinguishing between pepper and potato leaves may be more challenging as they are dramatically underrepresented relative to images of tomato leaves.  It may also be challenging to learn to identify healthy potato leaves, as there is a very small number of health leaves in the dataset.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%        DATA PREPROCESSING
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Preprocessing}

\subsection{Segmentation into Training, Test, and Validation sets}
As a first step, the dataset was segmented into 3 subsets: a training set, a validation set, and a test set.  The split was performed randomly, and the training, validation, and test sets represent 80\%, 10\% and 10\% of the dataset, respectively. The datasets were segmented in such a way as to preserve the underlying class distributions of plant types. % as outlined in section \ref{sec:dataset}. Commented out by Nick, Don't think it is needed in this case!

%processing of the data - image segmentation
The goal of pre-processing the data was to remove background to just leave the leafs for the CNN network. The expected output of the CNN is believed to classify more accurately given an image without a background. The image dataset for each plant and corresponding disease follows a similar template comprised of a light beige speckle background with leaf in the image center. The shadow casted by the leaf varies with the angle of illumination in each image. Following the literature detailed inside of  \cite{leafSEG}, the following segmentation algorithms were tested across all the leaves; edge based detection, thresholding detection, region based detection, clustering and watershed. For many of these algorithms, both the RGB and HSV color spaces were implemented due to the belief from \cite{hsvVSrgb} that different color spaces could actual alter and improve the resulting segementation of an object in an image. All of the different segmentation techniques are controlled through a file named RemoveBackgroundController.py, which is called by a jupyter notebook file for better visualization. The subsequent sections will briefly cover results tested which were not implemented for the final leaf segmentation, while the sections which were implemented for the final segmentations will be covered in greater detail.


% Edge detection!
\subsection{Edge Based Detection}
The first technique for segmenting the leaf from the background was a canny edge based detection method coupled with a hole filling filter upon the RGB color space of images. Multiple variances for the Gaussian filter were tested, with the most successful attempt being at a variance of 1 on the blue monochromatic image of a leaf. Figure \ref{fig:variance} shows the effect of variance upon the edge detection algorithm. One of the main issues with this techinique was the lack of edge linking, a common issue with this algorithm. This could have been fixed with a dilation and erosion filter however the shape of the leaf would have been altered by the convolutional kernel applied. This could have had an adverse affect on the CNN classification and was therefore not deemed not appropriate for our application.

\begin{figure}[H]
	\includegraphics[width=\columnwidth]{figs/Testing/EdgeDetectingVariance.PNG}
	\caption{Edge Based Detection on Blue Image with different variances }
	\label{fig:variance}
\end{figure}

% region growing!
\subsection{Region Growing}
Another technique which was implemented was region growing. The region growing algorithm was very sensitive on the starting seed point. Luckily the leaf was always centered in the image therefore a seed point was chosen to be in the middle of the image with size [5,5] and a multiplier of 2 for the standard deviation. This method provided good results for many types of leaves, however it was not deemed to be the best in any particular case. Figure \ref{fig:regiongrowing} shows the output of the region growing technique for the hue channel, which provided the best results.

\begin{figure}[H]
	\includegraphics[width=\columnwidth]{figs/Testing/RegionGrowing.png}
	\caption{Region Growing Detection on Hue Channel }
	\label{fig:regiongrowing}
\end{figure}

% Watershed
\subsection{Watershed}
Watershed technique was one of the more disappointing algorithms which was tested on the dataset. It performed as expected and oversegmented the images, provided too many objects, and was not broad enough for the images that were provided. The oversegmentation was caused by the algorithm getting stuck in local minimas inside the image, rather than finding the global minima as was originally intended. Figure \ref{fig:watershed} below shows the best outcome of the watershed filter that was achieved on the hue channel. The next steps that would have been take would be to input an gradient image into the filter rather than the original image however, the gradient would have also increased the noise in the image therefore more local minimas would be present. This would not solve the issue completely but might help in the segmentation, however better methods have already been tested therefore it was put aside for the remainder of testing.

\begin{figure}[h]
	\includegraphics[width=\columnwidth]{figs/Testing/Watershed.PNG}
	\caption{Watershed on Hue Channel }
	\label{fig:watershed}
\end{figure}

\subsection{Healthy Leaf -- Thresholding Detection}

Figure \ref{fig:healthyLeaf} presents an example of a healthy leaf image with threshold detection. There are three main classes within Figure \ref{fig:healthyLeaf}: leaf, background, and shadow. Depending on the illumination angle, small shadow area appears on the leaf surface due to leaf texture.
\begin{figure}[b]
	\includegraphics[width=40mm, height = 40mm]{figs/Healthy/health.JPG}
	\caption{Healthy Leaf: Three distinct classes within image}
	\label{fig:healthyLeaf}
\end{figure}

The red, green, and blue channel of the RGB image did not provide clear distinction between the three classes and thus provided low accuracy segmentation result. Hue-Saturation-Value (HSV) colour space provided more accurate segmentation as shown in Figure \ref{fig:healthyHSV}.

\begin{figure}[b]
	\centerline{\includegraphics[width=\columnwidth]{figs/Healthy/HSV.png}}
	\caption{Hue-Saturation-Value of healthy leeaf}
	\label{fig:healthyHSV}
\end{figure}

Otsu threshold method was used for hue channel, and the resulting binary image separated the background but included the leaf with the shadow. In order to remove the shadow, an upper threshold (Otsu) and lower threshold were required. The lower threshold value is determined using the histogram of the hue channel. The gradient between each successive bin is calculated and the first large gradient value represents the transition from shadow pixels to leaf pixels. Furthermore, assuming the number of leaf pixel is greater than shadow, the gradient has should be positive. The lower threshold value is then derived using these two assumptions as shown in Figure \ref{fig:healthyThresh}.

\begin{figure}
	\centerline{\includegraphics[width=80mm, height = 80mm]{figs/Healthy/health_Thresh.png}}
	\caption{The two thresholds used to separate the three classes in healthy leaf image}
	\label{fig:healthyThresh}
\end{figure}

The result from the two threshold contains small specks around the leaf which are removed using median filter and image label. The final mask is applied to the RGB channel from original image to produce result show in Figure \ref{fig:finalImage}.

The two-threshold algorithm performance was validate via visual inspection on 50-80 images. Once segmentation performance was deemed acceptable, the method was applied to the entire healthy dataset.

\begin{figure}[H]
	\centerline{\includegraphics[width=\columnwidth]{figs/Healthy/healthy_final.png}}
	\caption{Segmentation Result}
	\label{fig:finalImage}
\end{figure}

\subsection{Disease Leaf -- Clustering}
Figure \ref{fig:dLeaf} presents an example of disease leaf. There are four main classes within Figure \ref{fig:dLeaf}: leaf, background, shadow, and disease symptom spot. Depending on the illumination angle, there are small shadows on the leaf surface as well as symptom spots. The symptom spots vary in colour depending on the specific disease from black to pale yellow.

\begin{figure}[H]
	\centerline{\includegraphics[width=40mm, height = 40mm]{figs/Disease/disease.JPG}}
	\caption{Disease Leaf}
	\label{fig:dLeaf}
\end{figure}

The threshold method used previously for healthy leaf resulted in a poor segmentation for diseased leaves. The reason for the poor performance is shown in Figure \ref{fig:dLeafHue}, the symptom spot have very similar hue values as shadow. The resulting segmentation is missing large chunks of leaf and is hard to recover via post-processing.

\begin{figure}[H]
	\centerline{\includegraphics[width=40mm, height = 40mm]{figs/Disease/hueDisease.png}}
	\caption{Hue channel of disease leaf}
	\label{fig:dLeafHue}
\end{figure}

Instead of the two threshold method, K-Mean clustering algorithm is applied to the hue image to segment the image into two clusters followed by median filter and binary hole filling morphology operation. The results are shown in Figure \ref{fig:dKmeans}.

\begin{figure}[H]
	\centerline{\includegraphics[width=\columnwidth]{figs/Disease/kmeansDisease.png}}
	\caption{K Mean Clustering and Final Mask}
	\label{fig:dKmeans}
\end{figure}

The resulting mask is not as precise as mask generated for healthy leaves as the hue value for leaves and shadows are considered as a single cluster. Morphology operations such as dilation and erosion are applied given shadow occur at leaf edge. However, the irregular pattern of shadows (due to illumination) result in mask that still contains shadow. An example of final segmented image is shown in Figure \ref{fig:dFinal}

\begin{figure}[H]
	\centerline{\includegraphics[width=40mm, height = 40mm]{figs/Disease/dFinal.png}}
	\caption{Final segmented disease leaf}
	\label{fig:dFinal}
\end{figure}

The result of disease leaf segmentation is poorer than the healthy leaf, however it was deemed sufficient for a trial run using CNN. Image segmentation by itself is an application of machine learning, and a neural network would probably have provided much better segmentation performance. The purpose of this project however is not image segmentation but classification of leaf into healthy and diseased.  The plan is to continue improving the initial segmented dataset while training proposed CNN model and measure how the subsequent CNN classification performance has improved.


\subsection{PyTorch Dataset Class}
A PyTorch Dataset class was written to enable data loading and batching.  The implemented class closely follows the class laid out in the PyTorch beginner tutorials\cite{b1}.  PyTorch transform classes were also implemented to ensure all images were rescaled to the same size following the same tutorial.

The class enables access arbitrary dataset items (by index) or mini-batches the implementation of stochastic gradient descent.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              PLANT ID NET
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Plant ID Net}
As a first exploratory step a simple CNN was constructed with the goal of learning to identify plant leaf types from the dataset.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%              BIBLIOGRAPHY
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{thebibliography}{00}

\bibitem{appleCNN} P. Jiang, Y. Chen, B. Liu, D. He and C. Liang. (2019). \emph{Real-Time Detection of Apple Leaf Diseases Using Deep Learning Approach Based on Improved Convolutional Neural Networks}, IEEE Access, vol. 7, pp. 59069-59080, 2019.

\bibitem{mangoCNN} U. P. Singh, S. S. Chouhan, S. Jain and S. Jain. (2019). \emph{Multilayer Convolution Neural Network for the Classification of Mango Leaves Infected by Anthracnose Disease}, in IEEE Access, vol. 7, pp. 43721-43729, 2019.

\bibitem{alexNet} A. Krizhevsky, I. Sutskever and G. Hinton, \emph{ImageNet classification with deep convolutional neural networks}, Communications of the ACM, vol. 60, no. 6, pp. 84-90, 2012.

\bibitem{maizeCNN} X. Zhang, Y. Qiao, F. Meng, C. Fan and M. Zhang, \emph{Identification of Maize Leaf Diseases Using Improved Deep Convolutional Neural Networks}, in IEEE Access, vol. 6, pp. 30370-30377, 2018.

\bibitem{GoogLeNet} C. Szegedy, W. Liu, Y. Jia, P. Sermanet, S. Reed, D. Anguelov, D. Erhan, V. Vanhoucke, and A. Rabinovich. \emph{Going deeper with convolutions}. In Proceedings of the IEEE conference on computer vision and pattern recognition, pp. 1-9. 2015.

\bibitem{b1} S. Chilamkurthy (1991, Jan. 11). \emph{Writing Custom Datasets, Dataloaders and Transforms}  \emph{PyTorch}. [Online]. Available:\underline{https://pytorch.org/tutorials/beginner/data\_loading\_tutorial.html} accessed on 2020-03-18

\bibitem{leafSEG} A. Janwale. (2017). \emph{Plant Leaves Image Segmentation Techniques: A Review. International Journal of Computer Sciences and Engineering}. 5. 147-150. [Online]. Available:\underline{https://www.researchgate.net/publication/317370864\_Plant\_Leaves} \newline\underline{\_Image\_Segmentation\_Techniques\_A\_Review} accessed on 2020-02-28

\bibitem{hsvVSrgb} N. Mohd Ali. (2013). \emph{Performance Comparison between RGB and HSV Color Segmentations for Road Signs Detection. Applied Mechanics and Materials.} [Online]. Available: \underline{ 10.4028/www.scientific.net/AMM.393.550.}

\end{thebibliography}

\end{document}
