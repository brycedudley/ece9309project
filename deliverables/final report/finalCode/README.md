# Final Code Submission

> ECE9309B Final Project, Group 21

## A. Jupyter Notebooks

1. **PreProcessingBatch.ipynb**: Pre-process the images in the PlantVillage dataset. This file makes use of the RemoveBackgroundController.py python code.
2. **plantIDNet_segmented.ipynb**: Identify plant type on segmented images
3. **healthIDNet.ipynb**: Identify plant health on segmented images
4. **diseaseIDNet_Unsegmented.ipynb**: Identify disease type on unsegmented potato images
5. **diseaseIDNet_Segmented.ipynb**: Identify disease type on segmented potato images

## B. Python Files

1. **RemoveBackgroundController.py**: Used by a batch file to segment images. Really contains a lot of different segmentation functions.