# ECE9309B Final Project

> Group 21

## A. Folder Breakdown

- **/assignments**: Assignment code development (can ignore)
- **/dataset**: Final project processed dataset. "PlantVillage" folder holds all data processed by our project pre-processing (including categorization, image-processing, and analytics)
- **/deliverables**: *Final project submission deliverables* (includes organized final-code submission).
- **/notebooks**: Project development Jupyter notebooks used to load, train, and process machine-learning.
- **/tools**: Python files consisting of pre-processing tools that process dataset manipulation and image-processing.

## B. Dataset Used

Dataset obtained from [Kaggle.com Plant Disease Dataset](https://www.kaggle.com/emmarex/plantdisease).
